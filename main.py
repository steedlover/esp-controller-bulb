import network
import ubinascii
import json
from umqttsimple import MQTTClient
import machine
import uasyncio as asyncio
import utime
import re

pin = machine.Pin(2, machine.Pin.OUT, value=1)
wifi = None
mqtt = None
loop = asyncio.get_event_loop()
switchSignal = "switch"
addSubscriptionSignal = "subscribe"
subscriptions = []

fileconfig = "config.json"
# Config object
configDefault = {
    "MQTT_TOPIC_PING": "ping",
    "DEFAULT_CHARSET": "utf-8"
}


def getSignalMessage(objUpd=None, signup=False):
    msg = {
        "ip": config.get("WIFI_IP"),
        "mac": config.get("WIFI_MAC"),
        "type": config.get("DEVICE_TYPE")
    }
    if signup is False:
        msg.update({"id": config.get("MQTT_CLIENT_ID"),
                   "timestamp": utime.time()})
    if objUpd is not None:
        msg.update(objUpd)
    return json.dumps(msg)


def checkConfigFileAndReturn(obj):
    result = {}
    mandatoryFields = {
        "device": {
            "type": "DEVICE_TYPE"
        },
        "wifi": {
            "ssid": "WIFI_SSID",
            "passw": "WIFI_PAS"
        },
        "mqtt": {
            "ip": "MQTT_SERVER"
        }
    }
    optionalFields = {
        "device": {
            "static_id": "STATIC_ID"
        }
    }
    for key in mandatoryFields:
        for sub in mandatoryFields[key]:
            if key in obj and sub in obj[key] and obj[key][sub]:
                result.update({
                    mandatoryFields[key][sub]: obj.get(key).get(sub)
                })
            else:
                result = {}
                break
        else:
            continue
        break
    # Add all the optional fields
    # if the object is suitable
    if result:
        for key in optionalFields:
            for sub in optionalFields[key]:
                result.update({
                    optionalFields[key][sub]: obj.get(key).get(sub)
                })
    return result


def readConfig():
    updatedConfig = {}
    try:
        configFile = open(fileconfig, "r")
        parseConfigJson = json.loads(configFile.read())

        updatedConfig = checkConfigFileAndReturn(parseConfigJson)

        if not updatedConfig:
            configFile.close()
            raise AttributeError
        else:
            updatedConfig.update(configDefault)
            # updatedConfig.update({"SOURCE": parseConfigJson})
        configFile.close()
    except OSError:
        print("Config file is not accessible")
    except AttributeError:
        print("Config file has wrong format")
    finally:
        if not updatedConfig:
            return {}
        else:
            return updatedConfig


def writeNewConfig(config):
    try:
        configFile = open(fileconfig, "w")
        _bytes = configFile.write(config)
        print("Written ", _bytes)
        if _bytes <= 0:
            configFile.close()
            raise OSError
    except OSError:
        print("Config file is not accessible for writing")
    finally:
        if _bytes > 0:
            configFile.close()
            machine.reset()


def wifiConnect() -> None:
    global wifi
    wifi = network.WLAN(network.STA_IF)
    wifi.active(True)
    nets = wifi.scan()
    netExist = False
    attempts = 0

    if not wifi.isconnected():
        print('connecting to network...')

        for net in nets:

            if net[0].decode(
                config.get("DEFAULT_CHARSET")
            ) == config.get("WIFI_SSID"):
                netExist = True
                wifi.connect(config.get("WIFI_SSID"), config.get("WIFI_PAS"))
                while not wifi.isconnected() and attempts < 4:
                    utime.sleep(3)
                    attempts += 1
                    pass
        # If net with SSID was not found on scan
        if not netExist:
            print(config.get("WIFI_SSID"), "is not accessible")
            return None
        # If still disconnected
        elif not wifi.isconnected():
            print("Can't connect to", config.get("WIFI_SSID"), ". Disconnect")
            wifi.disconnect()

    if wifi.isconnected():
        # Update config variable
        config.update({
            "WIFI_IP": wifi.ifconfig()[0],
            "WIFI_MAC": ubinascii.hexlify(
                wifi.config('mac'), ':'
            ).decode(config.get("DEFAULT_CHARSET")),
        })
        print('network config: ', wifi.ifconfig())

    return True


def serverConfirmation(mode: int, signal: str, action=None, args=None) -> None:
    global mqtt
    spec_msg = {"state": 'on' if mode == 0 else 'off', "signal": signal}
    signalType = switchSignal
    if action == addSubscriptionSignal:
        signalType = addSubscriptionSignal
    if action is not None:
        signalType = action
    if args is not None:
        spec_msg.update({"args": args})
    json_msg = getSignalMessage(spec_msg)
    print('confirm/' + config.get("DEVICE_TYPE") + '/' +
          config.get("MQTT_CLIENT_ID") + '/' + signalType)
    mqtt.publish('confirm/' + config.get("DEVICE_TYPE") + '/' +
                 config.get("MQTT_CLIENT_ID") + '/' +
                 signalType,
                 json_msg
                 )


def mqttOnMessage(topic: bytes, msg: bytes) -> None:
    global subscriptions, addSubscriptionSignal, switchSignalDefault

    t = topic.decode(config.get("DEFAULT_CHARSET"))
    print(t, ": Input topic")
    mode = -1
    signalPrefix = config.get("DEVICE_TYPE", "") + "/"
    signalPrefix += config.get("MQTT_CLIENT_ID", "") + "/"
    arg = ""
    s = ""
    _msg = msg.decode(config.get("DEFAULT_CHARSET"))
    if t == config.get("WIFI_IP") + "/set_client_id":
        config["MQTT_CLIENT_ID"] = _msg
    elif (t == signalPrefix + addSubscriptionSignal):
        subscriptions.append(_msg)
    elif t == signalPrefix + 'on':
        mode = togglePin(1)
        s = "on"
    elif t == signalPrefix + 'off':
        mode = togglePin(-1)
        s = "off"
    elif t == signalPrefix + 'toggle':
        mode = togglePin(0)
        s = "toggle"
    elif t.startswith(signalPrefix + 'blink'):
        parseArr = t.split("/")
        arg = int(parseArr[-1])
        mode = blink(arg)
        s = "blink"
    elif t == signalPrefix + 'new_config':
        writeNewConfig(_msg)
        s = "new_config"
    elif t == signalPrefix + 'reset':
        machine.reset()
        s = "reset"

    if mode >= 0:
        serverConfirmation(mode, s, None, {"value": str(arg)})
    return


def mqttConnect(cid=""):
    global mqtt
    # Read certificate
    # certFile = open(config.get("MQTT_CERT"), "rb")
    # And key
    # keyFile = open(config.get("MQTT_KEY"), "rb")

    # sslParams = {"key": keyFile.read(),
    # "cert": certFile.read(),
    # "server_side": True,
    # }

    mqtt = MQTTClient(
        cid,
        config.get("MQTT_SERVER"),
        # ssl=True,
        # ssl_params = sslParams,
    )

    try:
        mqtt.connect()

        mqtt.set_callback(mqttOnMessage)
        # If the client ID is defined in config.json
        # no need to request server for an ID
        # but the device should be registered
        signupMsg = getSignalMessage(
            {"static_id": config.get("STATIC_ID")}, True)
        if not config.get("MQTT_CLIENT_ID"):
            mqtt.publish('signup', signupMsg)
            # Subscribe for setting clientID
            mqtt.subscribe(config.get("WIFI_IP") + '/set_client_id')
            # Strange string, the previous subscribe
            # line doesn't work without it!!!
            mqtt.subscribe("none")
    except OSError:
        print("Can't connect to MQTT server")
        mqtt = None

    return


def togglePin(val: int) -> int:
    # Pin should be global otherwise it works unpredictable
    global pin
    if val < 0:
        val = 1
    elif val > 0:
        val = 0
    elif val == 0:
        val = not pin.value()
    pin.value(val)
    return val


def blink(delay: int) -> None:
    # Pin should be global otherwise it works unpredictable
    global pin
    # min = config.get("SIGNALS_INCOMING")["blink"]["min"]  # milliseconds
    # max = config.get("SIGNALS_INCOMING")["blink"]["max"]  # milliseconds

    # if delay < min:
    # delay = min

    # if delay > max:
    # delay = max

    if pin.value():
        _on = 0
        _off = 1
        pin.value(_on)
        serverConfirmation(
            _on, "blink",
            None,
            {"value": str(delay), "in_progress": True}
        )
        utime.sleep(delay / 1000)
        pin.value(_off)

    return _off


async def ping() -> None:
    while True:
        mqtt.publish(config.get("MQTT_TOPIC_PING") + '/' +
                     config.get("DEVICE_TYPE") + '/' +
                     config.get("MQTT_CLIENT_ID"), getSignalMessage())

        await asyncio.sleep(2)


def closeConnection():
    global wifi
    try:
        while wifi.isconnected():
            wifi.disconnect()
            wifi.active(False)
            machine.idle()
            utime.sleep(1)
        print("Wifi connection closed")
    except OSError:
        print("OSError on disconnection")


async def checkMsg() -> None:
    global mqtt
    try:
        while True:
            mqtt.check_msg()
            await asyncio.sleep(0.2)
    except OSError:
        closeConnection()


async def checkSubscriptions() -> None:
    global mqtt, subscriptions
    try:
        while True:
            if len(subscriptions):
                for sub in subscriptions:
                    mqtt.subscribe(config.get("DEVICE_TYPE", "") + "/" +
                                   config.get("MQTT_CLIENT_ID", "") + "/" + sub)
                    serverConfirmation(pin.value(), addSubscriptionSignal,
                                       addSubscriptionSignal, {"value": sub})
                subscriptions = []
            await asyncio.sleep(0.2)
    except OSError:
        closeConnection()


config = readConfig()

if config:
    config.update(configDefault)

    wifiConnect()

    if wifi.isconnected():

        mqttConnect()

        if mqtt is not None:
            attempts = 0
            while "MQTT_CLIENT_ID" not in config and attempts < 4:
                attempts += 1
                utime.sleep(3)

            if "MQTT_CLIENT_ID" in config and config["MQTT_CLIENT_ID"] is not "":
                print("Client ID is ready", config["MQTT_CLIENT_ID"])
                # Subscribe for the subscription signal
                mqtt.subscribe(config.get("DEVICE_TYPE") + "/" +
                               config.get("MQTT_CLIENT_ID") + "/" +
                               addSubscriptionSignal)
                # Confirm getting device ID
                json_msg = getSignalMessage()
                mqtt.publish("confirm/" + config.get("DEVICE_TYPE") + "/" +
                             config.get("MQTT_CLIENT_ID") + "/set_client_id",
                             json_msg
                             )
                loop.create_task(checkMsg())
                loop.create_task(checkSubscriptions())
                # Start the endless ping
                loop.create_task(ping())
                loop.run_forever()
            else:
                err = "Can't retrieve an ID of the machine from "\
                    "the MQTT server"
                print(err)
        else:
            closeConnection()
