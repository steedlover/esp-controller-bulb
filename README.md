ESP8266 controllers

## Clone the project

```bash
git clone git@bitbucket.org:steedlover/esp-controller-bulb.git ~/<project_folder>
cd ~/<clone_folder>
```

## Deploy to device for Linux users:

### Erase flash

_PORT_ here for example /dev/ttyUSB0

```bash
esptool.py --port PORT --baud 115200 erase_flash
```

#### Upload custom firmware

```bash
esptool.py --port PORT --baud 115200 write_flash --flash_size=detect 0 ./firmware/esp8622.bin
```

#### Copy the config file

```bash
ampy --port PORT --baud 115200 put ./config.json
```

#### And the application

```bash
ampy --port PORT --baud 115200 put ./main.py
```

## Device signals

#### Subscription

Device subscribed for the following topics:

- `IP/set_client_id`

  Device wait for ID using current ip address, id will be
  passed as string argument.  
   Example: `bulb_005`

- `DEVICE_TYPE/DEVICE_ID/on`
- `DEVICE_TYPE/DEVICE_ID/off`
- `DEVICE_TYPE/DEVICE_ID/toggle`

  Change current state. If on -> off and vise versa

- `DEVICE_TYPE/DEVICE_ID/blink/<delay:int>`

  Blink the lamp with a delay. You can make it longer or shorter
  by changing the argument after slash. Keep in mind,
  the number in milliseconds.

- `DEVICE_TYPE/DEVICE_ID/new_config`

  Device waits for new config stringified line as argument  
   Example: `"{"id":"123","arg":"text"}"`

- `DEVICE_TYPE/DEVICE_ID/reset`

  Just reset device

## Communication device with the server

#### Request for server to register device

When device is loaded it sends request for signing
up.  
Topic: `signup` and stringified object as message

Example:

```bash
mqtt.publish("signup",
  json.dumps({
    "ip": "129.168.0.5", // Current device ip
    "mac": "98:22:ef:94:86:ed", // Device's mac address
    "type": "bulb", // Device's type
    "static_id": "05", // Optional attribute, set if static id needed
  })
)
```

#### Subscribe for setting client ID

```bash
mqtt.subscribe(':ip/set_client_id')
```

> msg = device ID

#### Confirmation of getting ID

```bash
mqtt.publish("confirm/:type/:id/set_client_id", {
  "ip": "129.168.0.5", // Current device ip
  "mac": "98:22:ef:94:86:ed", // Device's mac address
  "type": "bulb", // Device's type
  "id": "bulb_005",
  "timestamp": 179
})
```

#### Subscribe for getting signals

```bash
mqtt.subscribe(":type/:id/subscribe")
```

> msg = signal

#### Ping

```bash
mqtt.publish("ping/:type/:id", {
  "ip": "129.168.0.5", // Current device ip
  "mac": "98:22:ef:94:86:ed", // Device's mac address
  "type": "bulb", // Device's type
  "id": "bulb_005",
  "timestamp": 179
})
```

#### Confirm the server about changing of a state

When state is being changed, device need to inform the server
about the successfull changing.  
Topic: `confirm/:type/:id/switch` and stringified object as message  
Example:

```bash
mqtt.publish("confirm/" + config.get("MQTT_CLIENT_ID") + '/' +
    config.get("DEVICE_TYPE") + '/switch',
    json.dumps({
        "id": config.get("MQTT_CLIENT_ID"),
        "ip": config.get("WIFI_IP"),
        "mac": config.get("WIFI_MAC"),
        "type": config.get("DEVICE_TYPE"),
        "state": 'on' if mode == 0 else 'off',
        "timestamp": utime.time(),  // device's uptime
    })
)
```
